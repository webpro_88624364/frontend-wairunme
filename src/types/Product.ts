export default interface Product {
  id?: number;
  name: string;
  type: string;
  size: number;
  price: number;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
